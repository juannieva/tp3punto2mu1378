package tp3punto2;

public class  DineroInsuficienteException extends RuntimeException {
    
    public DineroInsuficienteException(){
        super("DINERO INSUFICIENTE");
    }
}
