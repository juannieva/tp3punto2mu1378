package tp3punto2;

public class Pago {
    private Orden orden;
    
    public Pago(Orden orden){
        this.orden=orden;
    }

    public Orden getOrden(){
        return orden;
    }
    
}
