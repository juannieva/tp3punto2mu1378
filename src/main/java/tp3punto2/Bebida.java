package tp3punto2;
import java.math.BigDecimal;

public class Bebida {
    private String nombreBebida;
    private BigDecimal costo;
    private String descripcion;
    private Boolean alcohol;

    public Bebida(String nombreBebida, BigDecimal costo, String descripcion, Boolean alcohol) {
        this.nombreBebida = nombreBebida;
        this.costo = costo;
        this.descripcion= descripcion;
        this.alcohol= alcohol;
    }

    public String getNombreBebida() {
        return nombreBebida;
    }

    public void setNombreBebida(String nombreBebida) {
        this.nombreBebida = nombreBebida;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Boolean alcohol) {
        this.alcohol = alcohol;
    }
    
    
}
