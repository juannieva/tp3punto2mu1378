package tp3punto2;

import java.math.BigDecimal;

public class Tarjeta extends Pago{
    private String nombreyApellidotitular;
    private BigDecimal nroTarjeta;
    private Integer codigodeSeguridad;
    
    public Tarjeta(Orden orden, String nombreyApellidotitular, BigDecimal nroTarjeta, Integer codigodeSeguridad) {
        super(orden);
        this.nombreyApellidotitular = nombreyApellidotitular;
        this.nroTarjeta = nroTarjeta;
        this.codigodeSeguridad = codigodeSeguridad;
    }

    public String getNombreyApellidotitular() {
        return nombreyApellidotitular;
    }

    public void setNombreyApellidotitular(String nombreyApellidotitular) {
        this.nombreyApellidotitular = nombreyApellidotitular;
    }

    public BigDecimal getNroTarjeta() {
        return nroTarjeta;
    }

    public void setNroTarjeta(BigDecimal nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public Integer getCodigodeSeguridad() {
        return codigodeSeguridad;
    }

    public void setCodigodeSeguridad(Integer codigodeSeguridad) {
        this.codigodeSeguridad = codigodeSeguridad;
    }
    
}
