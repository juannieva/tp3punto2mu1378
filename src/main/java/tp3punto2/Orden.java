package tp3punto2;

import java.util.ArrayList;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Orden{
    private Integer cantComenzales;
    private Integer nroMesa;
    private String nombreMozo;
    private ArrayList<Plato> platos;
    private ArrayList<Bebida> bebidas;
    private LocalDateTime horaLlegada;
    private LocalDateTime horaSalida;
    private BigDecimal costoTotal;

    public Orden(Integer cantComenzales, Integer nroMesa,String nombreMozo, LocalDateTime horaLlegada, LocalDateTime horaSalida){
        this.cantComenzales=cantComenzales;
        this.nroMesa=nroMesa;
        this.nombreMozo=nombreMozo;
        this.horaLlegada=horaLlegada;
        this.horaSalida=horaSalida;
        setPlato();
        setBebida();
    }
    
    public void setPlato(){
        platos = new ArrayList<Plato>();
    }

    public void setBebida(){
        bebidas = new ArrayList<Bebida>();
    }

    public void agregarPlato(String nombreComida,String descripcion,BigDecimal costo){
        Plato plato = new Plato(nombreComida,descripcion,costo);
        platos.add(plato);
        setcostoTotal();
    }
    public void agregarBebida(String nombreBebida,BigDecimal costo,String descripcion,Boolean alcohol){
        Bebida bebida = new Bebida(nombreBebida,costo,descripcion,alcohol);
        bebidas.add(bebida);
        setcostoTotal();
    }


    public Integer getNroMesa() {
        return nroMesa;
    }

    public void setNroMesa(Integer nroMesa) {
        this.nroMesa = nroMesa;
    }

    public LocalDateTime getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(LocalDateTime horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public LocalDateTime getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(LocalDateTime horaSalida) {
		this.horaSalida = horaSalida;
	}

    public Integer getCantComenzales() {
        return cantComenzales;
    }

    public void setCantComenzales(Integer cantComenzales) {
		this.cantComenzales = cantComenzales;
    }

    public BigDecimal getCostoTotal() {
        return costoTotal;
    }

    public void setcostoTotal(){
        BigDecimal platosParcial= new BigDecimal(0);
        BigDecimal bebidasParcial= new BigDecimal(0);
        for (Plato var: platos) {
            platosParcial= platosParcial.add(var.getCosto());
    }
        for (Bebida var: bebidas) {
         bebidasParcial= bebidasParcial.add(var.getCosto());
    }
        this.costoTotal= platosParcial.add(bebidasParcial);
    }

    public String getNombreMozo() {
        return nombreMozo;
    }

    public void setNombreMozo(String nombreMozo) {
        this.nombreMozo = nombreMozo;
    }
}   