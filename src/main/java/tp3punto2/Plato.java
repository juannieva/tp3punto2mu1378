package tp3punto2;
import java.math.BigDecimal;

public class Plato {
    private String nombreComida;
    private String descripcion;
    private BigDecimal costo;

    public Plato(){
    }
    public Plato(String nombreComida,String descripcion, BigDecimal costo) {
        this.nombreComida = nombreComida;
        this.descripcion = descripcion;
        this.costo = costo;
    }

    public String getNombreComida() {
        return nombreComida;
    }

    public void setNombreComida(String nombreComida) {
        this.nombreComida = nombreComida;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
