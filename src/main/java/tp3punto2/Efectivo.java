package tp3punto2;

import java.math.BigDecimal;

public class Efectivo extends Pago {
    private BigDecimal montoRecibido;
    private BigDecimal vuelto;

    public Efectivo(Orden orden, BigDecimal montoRecibido){
        super(orden);
        this.montoRecibido=montoRecibido;
        Integer comparacion=montoRecibido.compareTo(orden.getCostoTotal());
        if(comparacion<0){
            throw new DineroInsuficienteException();
        }
        setVuelto();
    }

    public BigDecimal getMontoRecibido() {
        return montoRecibido;
    }

    public void setMontoRecibido(BigDecimal montoRecibido) {
        this.montoRecibido = montoRecibido;
    }

    public BigDecimal getVuelto() {
        return vuelto;
    }

    public void setVuelto() {
        Orden orden;
        orden=getOrden();
        vuelto=montoRecibido.subtract(orden.getCostoTotal());
    }
    
}
