package tp3punto2;

import java.math.BigDecimal;

public class TarjetaDebito extends Tarjeta {

    public TarjetaDebito(Orden orden, String nombreyApellidotitular, BigDecimal nroTarjeta, int codigodeSeguridad) {
        super(orden, nombreyApellidotitular, nroTarjeta, codigodeSeguridad);
    }
    
}
