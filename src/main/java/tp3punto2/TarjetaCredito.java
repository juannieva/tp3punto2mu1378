package tp3punto2;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TarjetaCredito extends Tarjeta {
    private BigDecimal interes;
    private BigDecimal cuotas;
    private BigDecimal montoCuotas;
    private BigDecimal montoFinanciado;

    public TarjetaCredito(Orden orden, String nombreyApellidotitular, BigDecimal nroTarjeta, Integer codigodeSeguridad,
            BigDecimal interes, BigDecimal cuotas){
        super(orden, nombreyApellidotitular, nroTarjeta, codigodeSeguridad);
        this.interes = interes.divide(new BigDecimal("10"),2,RoundingMode.HALF_UP);
        this.cuotas = cuotas;
        setmontoFinanciado();
        setmontoCuotas();
    }

    public void setmontoFinanciado(){
        Orden orden1=getOrden();
        this.montoFinanciado= orden1.getCostoTotal().multiply(interes);
    }

    public void setmontoCuotas(){

        montoCuotas = (montoFinanciado.divide(cuotas));
    }

    public BigDecimal getInteres() {
        return interes;
    }

    public void setInteres(BigDecimal interes) {
        this.interes = interes;
    }

    public BigDecimal getCuotas() {
        return cuotas;
    }

    public void setCuotas(BigDecimal cuotas) {
        this.cuotas = cuotas;
    }

    public BigDecimal getMontoCuotas() {
        return montoCuotas;
    }

    public void setmontoCuotas(BigDecimal montoCuotas) {
        this.montoCuotas = montoCuotas;
    }
    
}
