package tp3punto2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;


public class OrdenTest {

    @Test
    public void creaciondeOrdenyAgregar2comenzalesySumarSusOrdenes(){        
        LocalDateTime fechaEntrada=LocalDateTime.parse("2020-09-28T10:00:00");
        LocalDateTime fechaSalida=LocalDateTime.parse("2020-09-28T14:00:00");
        Orden orden1 = new Orden(2,23,"Jose Rodriguez",fechaEntrada,fechaSalida);
        orden1.agregarBebida("CocaCola", new BigDecimal(150),"Gaseosa levemente gasificada",false);
        orden1.agregarPlato("Fideos con Salsa","Fideos con salsa bolognesa y queso", new BigDecimal(300));
        orden1.agregarPlato("Bife con Pure","Bife a la criolla con pure de papas condimentado", new BigDecimal(250));
        assertEquals(new BigDecimal(700),orden1.getCostoTotal());
    }
    
}
