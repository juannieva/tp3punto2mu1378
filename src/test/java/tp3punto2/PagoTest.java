package tp3punto2;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import static org.junit.Assert.*;
import org.junit.Test;

public class PagoTest {

    @Test
    public void PagarOrdenyaCreadaconEfectivo(){
    LocalDateTime fechaEntrada=LocalDateTime.parse("2020-09-28T10:00:00");
    LocalDateTime fechaSalida=LocalDateTime.parse("2020-09-28T14:00:00");
    Orden orden1 = new Orden(2,23,"Jose Rodriguez",fechaEntrada,fechaSalida);
    orden1.agregarBebida("CocaCola", new BigDecimal(150),"Gaseosa levemente gasificada",false);
    orden1.agregarPlato("Fideos con Salsa","Fideos con salsa bolognesa y queso", new BigDecimal(300));
    orden1.agregarPlato("Bife con Pure","Bife a la criolla con pure de papas condimentado", new BigDecimal(250));
    Efectivo efectivo=new Efectivo(orden1,new BigDecimal(1150));
    assertEquals(new BigDecimal("450"),efectivo.getVuelto());
    }

    @Test
    public void PagarOrdenyaCreadaconTarjetaDeCredito(){
    LocalDateTime fechaEntrada=LocalDateTime.parse("2020-09-28T10:00:00");
    LocalDateTime fechaSalida=LocalDateTime.parse("2020-09-28T14:00:00");
    Orden orden1 = new Orden(2,23,"Jose Rodriguez",fechaEntrada,fechaSalida);
    orden1.agregarBebida("CocaCola", new BigDecimal(150),"Gaseosa levemente gasificada",false);
    orden1.agregarPlato("Fideos con Salsa","Fideos con salsa bolognesa y queso", new BigDecimal(300));
    orden1.agregarPlato("Bife con Pure","Bife a la criolla con pure de papas condimentado", new BigDecimal(250));
    TarjetaCredito tarj = new TarjetaCredito(orden1, "Jose Rodriguez", new BigDecimal("1238726342"), 872, new BigDecimal("15"), new BigDecimal("3"));
    assertEquals(new BigDecimal("1238726342"),tarj.getNroTarjeta());
    assertEquals(new BigDecimal("350.00"),tarj.getMontoCuotas());
    }

@Test
public void PagarOrdenyaCreadaconTarjetaDeDebito(){
    LocalDateTime fechaEntrada=LocalDateTime.parse("2020-09-28T10:00:00");
    LocalDateTime fechaSalida=LocalDateTime.parse("2020-09-28T14:00:00");
    Orden orden1 = new Orden(2,23,"Jose Rodriguez",fechaEntrada,fechaSalida);
    orden1.agregarBebida("CocaCola", new BigDecimal(150),"Gaseosa levemente gasificada",false);
    orden1.agregarPlato("Fideos con Salsa","Fideos con salsa bolognesa y queso", new BigDecimal(300));
    orden1.agregarPlato("Bife con Pure","Bife a la criolla con pure de papas condimentado", new BigDecimal(250));
    TarjetaDebito tarj = new TarjetaDebito(orden1, "Jose Rodriguez", new BigDecimal("1238726342"), 872);
    assertEquals(new BigDecimal("1238726342"),tarj.getNroTarjeta());
    }
}